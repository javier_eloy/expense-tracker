<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::resource('/transaction','TransactionController');
Route::resource('/category','CategoryController');

Route::get('/transaction/{days}/{id?}/filter/',[ 'as' =>'transaction.filter' ,'uses' => 'TransactionController@filter']);
Route::get('/category/{id}/filter/',[ 'as' =>'category.filter' ,'uses' => 'CategoryController@filter']);

Route::post('login/auth',[ 'as' =>'login.auth' ,'uses' => 'AuthController@login']);  
Route::post('login/register',[ 'as' =>'login.register' ,'uses' => 'AuthController@register']);  
Route::get('login/destroy',[ 'as' =>'login.destroy' ,'uses' => 'AuthController@logout']);
	