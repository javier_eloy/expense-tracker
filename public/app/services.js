app.factory('Transaction',fTransaction)
   .factory('Categories',fCategories)
   .factory('ControlError',fControlError)
   .factory('SessionService',fSessionService)
   .factory('Login',fLogin)   
   .factory('AuthenticationService',fAuthenticationService)
   .filter('sumByKey',fsumByKey);
   
function fTransaction($resource,API_URL) 
{
	return $resource(API_URL + '/transaction/:id',
	      {'id':'@id'},
		  {
		  'query' : { method:'GET', isArray:true },
		  'update': { method:'PUT' },
		  'save'  : { method:'POST' },
		  'remove': { method:'DELETE'},
		  'filter': { method:'GET',params:{'days':'@days', 'id':'@id'}, isArray:true, url: API_URL + '/transaction/:days/:id/filter' }
		 });
}

function fCategories($resource,API_URL) 
{
	return $resource(API_URL + '/category/:id',
	      {'id':'@id'},
		  {
		  'query' : { method:'GET', isArray:true },
		  'update': { method:'PUT' },
		  'save'  : { method:'POST' },
		  'remove': { method:'DELETE'},
		  'filter': { method:'GET',params:{'id':'@id' }, isArray:true, url: API_URL + '/category/:id/filter' }
		 });
}

function fControlError() 
{	
	   return { 
	    show: function(status,message){	
			 switch(status){
				   case 500: return "Can not complete communication with backend";
				   case 401: return "Unauthorized access. Please login";
				   case 422: return "There was a problem with your submission: " + message;
				   default: return "Unknown Error: " + message;
			 } 
	      }
	  }
}
// Save credential
function fSessionService()
{  
		return{  
		  get:function(key){ 
			return sessionStorage.getItem(key); 
		  }, 
		  set:function(key,val){ 
			return sessionStorage.setItem(key,val); 
		  }, 
		  unset:function(key){ return sessionStorage.removeItem(key); 
		  } 
		}
}

// Authentication layer to encapsulate variables
function fAuthenticationService($http, SessionService) {

  'use strict';

  return {

    login: function (user) {
       SessionService.set('currentUser',user);
    },

    isLoggedIn: function () {
      return SessionService.get('currentUser') !== null;
    },
	
	logout: function () {
	   SessionService.unset('currentUser');
	   return null;
		 
	},
	
	current : function () {
	  return SessionService.get('currentUser');	
	}	
	
  };
}

// Login REST
function fLogin($http)
{  
		return{ 
		  auth:function(credentials){ 
			 return $http({
						method:'POST',
						url:'api/login/auth',
						params:credentials
					}); 
		  },
  		  logout: function() {
			 return $http({
						method:'GET',
						url:'api/login/destroy'
					});
		  },
		  register:function(user) {
			  return $http({
						method:'POST',
						url:'api/login/register',
						params:user
					});
		  }
		}
}

// Filter
function fsumByKey() 
{
        return function(data, key, condition, value) {
            if (typeof(data) === 'undefined' || typeof(key) === 'undefined') {
                return 0;
            }
			
            var sum = 0;
            for (var i = data.length - 1; i >= 0; i--) {
				if(data[i][key] == condition) {
                    sum += parseFloat(data[i][value]);
				}
            }

            return sum;
        };
}	