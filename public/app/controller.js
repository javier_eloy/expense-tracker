app.controller("HomeController", HomeController)
   .controller("LoginController",LoginController)
   .controller("RegisterController",RegisterController)
   .controller("DashController", DashController)
   .controller("TransactionController", TransactionController)
   .controller("CategoriesController",CategoriesController)
   .controller("NavController",NavController);

   
// Nav Controller
function NavController($scope, AuthenticationService, $location) 
{   
   
   $scope.islogged = function() {
	   return $scope.logged=AuthenticationService.isLoggedIn();  
   }
   
   $scope.logout = function() {
	      AuthenticationService.logout();				  
		  $location.path("/login");
   }
   
}   
   
// Home Controller 
function HomeController($scope) 
{
	  
	  
}

// Login Controller
function LoginController($scope,$http, Login, AuthenticationService, $location) 
{
	 $scope.loginSubmit = function () {
        Login.auth($scope.login)
		     .then(function(response){
				  if (response.data.id) {
					   AuthenticationService.login(angular.toJson(response.data));
					   $location.path("/");
				  } else {					  
				        AuthenticationService.logout();
					    $scope.message="Could not verify login";
				  }
			 },
			 function(err) {
				 $scope.message = err.statusText;
			 })
		
	 
	 }
	  
}

// Register Controller
function RegisterController($scope,$http, Login, AuthenticationService, $location) 
{
	 $scope.registerSubmit = function () {
		$scope.login.email = 'user@role';
		
        Login.register($scope.login)
		     .then(function(response){
				  if (response.data.id) {
					   AuthenticationService.login(angular.toJson(response.data));
					   $location.path("/");
				  } else {					  
				       AuthenticationService.logout();
					   $scope.message="Could not register";
				  }
				}, function (err) {				 
				     $scope.message = err.statusText; 
					 $(".alert").show()
				})
	 }	 
	  
}

// Dash Controller 
function DashController($scope,$http, Transaction, AuthenticationService, $filter, $route) 
{
  var user = angular.fromJson(AuthenticationService.current());
  $scope.transactions = [];

   // Load current list   
   var floadData=function(data){
		$scope.transactions=data;   		   
 
		var deposit=$filter('sumByKey')($scope.transactions,'type','0','amount');
		var withdrawal=$filter('sumByKey')($scope.transactions,'type','1','amount');		  
		var ctx = $("#myPieChart");
		var myPieChart = new Chart(ctx, {
		  type: 'pie',
		  data: {
				labels: ["deposit", "withdrawal"],
				datasets: [{
				  data: [deposit, withdrawal],
				  backgroundColor: ['#007bff', '#dc3545'],
				}],
			  },
		});
		$scope.total = deposit - withdrawal;
			
   };
   var filterdays=$('#filterTransaction').val();
   
   $(filterTransaction).change(function() {
		    $route.reload();
   });   
   
   // Load current list
   	Transaction
	    .filter({days:filterdays, id: (user.email != 'admin@role') ? user.id : 0},
		 floadData,
		 function(err){ $scope.message=ControlError.show(err.status,err.statusText); });
   
}

// Transaction Controller
function TransactionController($scope,$http,Transaction, Categories, ControlError, AuthenticationService, $route) 
{	   
	   var filterdays=$('#filterTransaction').val();
	   
	   $scope.user = angular.fromJson(AuthenticationService.current());
	   $scope.transactions = [];
	   $scope.message= "";		
	   
	   $(filterTransaction).change(function() {
		    $route.reload();
	   });

       // Load current list
        Transaction
		    .filter({days:filterdays, id: ($scope.user.email != 'admin@role') ? $scope.user.id : 0},
					function(data){ $scope.transactions=data; }, 
					function(err) { $scope.message=ControlError.show(err.status,err.statusText + err.errors.toString());});		 
	   
	   // Action to modal window
	    $scope.toggle = function(modalstate, id) {
			
		 $scope.modalstate = modalstate;

		 // Define options list
		 $scope.options = [{ name: "deposit", id: 0 }, { name: "withdrawal", id: 1 }];

		 // Load categories list
		 Categories
		   .filter({ id: ($scope.user.email != 'admin@role') ? $scope.user.id : 0 },
		            function(data){ $scope.categories=data; });
					
		 
         switch (modalstate) {
            case 'add':
			    $scope.transaction = [];
                $scope.form_title = "Add New Transaction";
                break;
            case 'edit':
                $scope.form_title = "Transaction Detail";
                $scope.id = id;

				// Load transaction to edit 
				Transaction.get({id : id}, function(response) {
					$scope.transaction = response;
				});
				
                break;
            default:
                break;
          }
          $('#myModal').modal('show');
       }
	   
	    //Save new record / update existing record
		$scope.Save = function(modalstate, id) {
					
			switch (modalstate) {
				case 'edit':
				
				  Transaction.update({id:id},$scope.transaction, function(response) {
					  $route.reload();
				  },
				  function(err){
					  $scope.message=ControlError.show(err.status,err.statusText + err.errors.toString());
				  });
				  break;	
				  
				case 'add':
				   				
				   Transaction.save( {
					   subject :$scope.transaction.subject,
					   category_id:$scope.transaction.category_id,					   
					   amount  :$scope.transaction.amount,
					   date    :$scope.transaction.date,
					   type    :$scope.transaction.type,
					   user_id :$scope.user.id 
					  }, function(response) {
						   $route.reload();
				      },
					   function(err){
						   console.log(err);
						   $scope.message=ControlError.show(err.status,err.statusText + JSON.stringify(err.data.errors));						   
					   });
				  break;

			}
		    $('#myModal').modal('hide');			
			
		
		}
		// Delete record
	   $scope.ConfirmDelete=function(id){
		  var isOk = confirm('¿Are you sure?');
		  if(isOk){
			  Transaction.remove({id: id},
			     function(response){	
					$route.reload();
				},
				 function(err) {
					$scope.message=ControlError.show(err.status,err.statusText);
				});
		  }
	   }
 }
 
 // Categories Controller 
 function CategoriesController($scope,$http,Categories,ControlError,AuthenticationService,$route) 
 {
	   $scope.user = angular.fromJson(AuthenticationService.current());			
	   $scope.categories = [];
	   $scope.message= "";
	   
       
	   // Load inital list	   
	    Categories
		   .filter({ id: ($scope.user.email != 'admin@role') ? $scope.user.id : 0 },
		            function(data){ $scope.categories=data; },
					function(err) { $scope.message=ControlError.show(err.status,err.statusText) });
	   
	   // Action to modal window
	   $scope.toggle = function(modalstate, id) {
	   $scope.modalstate = modalstate;

		// Define options list
		$scope.options = [{ name: "deposit", id: 0 }, { name: "withdrawal", id: 1 }];
        switch (modalstate) {
            case 'add':
			    $scope.category = [];
                $scope.form_title = "Add New Category";
                break;
            case 'edit':
                $scope.form_title = "Category Detail";
                $scope.id = id;

				// Load transaction to edit 
				Categories.get({id : id}, function(response) {
					$scope.category = response;
				});
				
                break;
            default:
                break;
          }
          $('#myModal').modal('show');
       }
	   
	   
	     //Save new record / update existing record
		$scope.Save = function(modalstate, id) {
		
			switch (modalstate) {
				case 'edit':
				
				  Categories.update({id:id},$scope.category, function(response) {
					 $route.reload();
				  },
				  function(err){
					  $scope.message=ControlError.show(err.status,err.statusText);
				  });
				  break;	
				  
				case 'add':
			
				   Categories.save( {
					   title   : $scope.category.title,
					   user_id : $scope.user.id
					  }, function(response) {
						  $route.reload();
				   },
				   function(err){
					   $scope.message=ControlError.show(err.status,err.statusText + JSON.stringify(err.data.errors));
				   });
				  break;

			}
		    $('#myModal').modal('hide');				
		}
		
		// Delete record
	   $scope.ConfirmDelete=function(id){
		  var isOk = confirm('¿Are you sure?');
		  if(isOk){
			  Categories.remove({id: id},
			     function(response){	
					$route.reload();
				},
				 function(err) {
					  $scope.message=ControlError.show(err.status,err.statusText);
				});
		  }
	   }
 }