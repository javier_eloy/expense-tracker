var app=angular.module("ExpenseTracker", ["ngResource","ngRoute"]);

app.constant('API_URL','http://localhost/expensetracker/public/api')
   .config(configure)
   .run(auth);

   
function configure($routeProvider) 
{
	   $routeProvider
		.when("/",{
			  controller: "HomeController",
			  templateUrl: "app/templates/home.tpl.html"
		})
		.when("/dash",{
			  controller: "DashController",
			  templateUrl: "app/templates/dash.tpl.html"
		})
		.when("/transaction",{
			controller: "TransactionController",
			templateUrl: "app/templates/transactions.tpl.html"
		})
		.when("/categories",{
			controller: "CategoriesController",
			templateUrl: "app/templates/categories.tpl.html"
		})
		.when("/login",{
			controller: "LoginController",
			templateUrl: "app/templates/login.tpl.html"
		})
		.when("/register",{
			controller: "RegisterController",
			templateUrl: "app/templates/register.tpl.html"
		})
		.otherwise({
			controller: "HomeController",
			templateUrl: "app/templates/home.tpl.html"
		});

  }
  
 function auth($rootScope, $location, AuthenticationService) {

  'use strict';
  
  // enumerate routes that don't need authentication
  var routesThatDontRequireAuth = ['/login', '/register'];

  // check if current location matches route  
  var routeClean = function (route) {
    return _.find(routesThatDontRequireAuth,
      function (noAuthRoute) {
          return (route == noAuthRoute); //!!str.startWith(route, noAuthRoute);
      });
  };

  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    // if route requires auth and user is not logged in
    if (!routeClean($location.url()) && !AuthenticationService.isLoggedIn()) {
        // redirect back to login
        $location.path('/login');
    }
  });
  
   // Global function 
   $rootScope.hasMessage = function(message) {
	    return (message);   
   }
  

}