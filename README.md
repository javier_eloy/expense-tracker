# README #


### Como se configura? ###

* Instalar laravel con la ultima version
* Configurar las variables para conexión a Base de Datos
* Generar la migración con el comando:
     php artisan migrate
* Inicializar la lista de usuarios con el comando:
     php artisan db:seed
* Ejecutar las pruebas con los usuarios:
    - admin (clave: admin) : Usuario administrador
	- user (clave: user)   : Usuario estandar


### Consideraciones ###

Aplicación para considerar pruebas con Laravel, AngularJS, Repository Pattern.

### Para mejorar ###

* El modulo de registro solamente pide una vez la clave. Se hizo unicamente para demostrar el ingreso de usuarios.
* Agregar actividades al administrador, como agregar/eliminar otros usuarios no administradores. (Se encuentra preparado para mejorar, son cambios menores) 
* Alguna definicion de reporte.