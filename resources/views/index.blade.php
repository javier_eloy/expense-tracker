<!DOCTYPE html>
<html ng-app="ExpenseTracker">
    <head>
        <title>Expense Tracker</title>

        <!-- Load Bootstrap CSS -->
        <link href="<?= asset('css/bootstrap.min.css') ?>" rel="stylesheet">
		<link href="<?= asset('css/sb-admin.min.css') ?>" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    </head>
  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
   <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="#">Expense Tracker</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{!! url('#!/dash') !!}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Transactions">
          <a class="nav-link" href="{!! url('#!/transaction') !!}">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Transactions</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Categories">
          <a class="nav-link" href="{!! url('#!/categories') !!}">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Categories</span>
          </a>
        </li>       
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
		    <i class="fa fa-fw fa-angle-left"></i>
		  </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto" ng-controller="NavController">          	  		   
	    <li class="nav-item">
		   <div class="input-group">
		      <span class="nav-link">Filter Transaction </span> 
			  <select id="filterTransaction"class="form-control form-control-sm">
				  <option value="0">(All)</option>
				  <option value="7">Last 7 days</option>
				  <option value="30">Last 30 days</option>
			  </select>
              <span class="input-group-append">
              </span>
           </div>
		</li>
        <li class="nav-item" ng-hide="islogged()">
          <a class="nav-link" href="{!! url('#!/login')!!}">
            <i class="fa fa-fw fa-sign-in"></i>Login
		  </a>
        </li>
        <li class="nav-item" ng-hide="!islogged()">
          <a class="nav-link" ng-click="logout()" >
            <i class="fa fa-fw fa-sign-out"></i>Logout
		  </a>
        </li>

		
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div ng-view></div>
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Javier Hernandez 2018</small>
        </div>
      </div>
    </footer>
	 <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
  </div>
        
	  
       <!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
        <script src="<?= asset('js/angularjs/angular.min.js') ?>"></script>
		<script src="<?= asset('js/angularjs/angular-route.min.js') ?>"></script>
		<script src="<?= asset('js/angularjs/angular-resource.min.js') ?>"></script>		
		
        <script src="<?= asset('js/jquery-3.3.1.min.js') ?>"></script>
		<script src="<?= asset('js/bootstrap.bundle.min.js') ?>"></script>
		<script src="<?= asset('js/jquery.easing.min.js') ?>"></script>
		<script src="<?= asset('js/Chart.min.js') ?>"></script>
		<script src="<?= asset('js/sb-admin.min.js') ?>"></script>
		<script src="<?= asset('js/underscore.js') ?>"></script>
		
        
        <!-- AngularJS Application Scripts -->
        <script src="<?= asset('app/app.js') ?>"></script>
		<script src="<?= asset('app/services.js') ?>"></script>
        <script src="<?= asset('app/controller.js') ?>"></script>
		
	</body>
</html>