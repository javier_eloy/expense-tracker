<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {			
            //
			$table->increments('id');
			$table->text('subject');
			$table->float('amount',8,2);
			$table->date('date');
			$table->integer('type'); // 0 deposit / 1 Withdrawal
			$table->integer('category_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('category');
    }
}
