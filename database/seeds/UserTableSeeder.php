<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->delete();
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@role',
            'password' => bcrypt('admin'),
        ]);
		
		 DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@role',
            'password' => bcrypt('user'),
        ]);

    }
}
