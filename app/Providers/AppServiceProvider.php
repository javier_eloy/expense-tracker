<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Repositories\TransactionInterface;
use App\Repositories\TransactionRepository;

use App\Repositories\CategoryInterface;
use App\Repositories\CategoryRepository;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
		Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
	  $this->app->singleton(TransactionInterface::class, TransactionRepository::class);
	  $this->app->singleton(CategoryInterface::class, CategoryRepository::class);
	  $this->app->singleton(UserInterface::class, UserRepository::class);
    }
}
