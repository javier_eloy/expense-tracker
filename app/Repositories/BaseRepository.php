<?php

namespace App\Repositories;
 
abstract class BaseRepository 
 {
    protected $model;
  
	public function getAll(){
		return $this->model->all();
	}
	public function find($id){
		return $this->model->find($id);
	}
	public function create(array $attributes){
		return $this->model->create($attributes);
	}
	public function update($id, array $attributes){
		return $this->model->find($id)->update($attributes);
				
	}
	public function destroy($id){
		return $this->model->findorfail($id)->delete();
	}

 
 }