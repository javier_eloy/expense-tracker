<?php

namespace App\Repositories;

use App\Repositories\BaseRepositoryInterface;

interface CategoryInterface extends BaseRepositoryInterface
{ 
    public function getAllFilter($id);
}