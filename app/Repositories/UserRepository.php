<?php 

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\TransactionInterface;
use App\User;
use Carbon\Carbon;

class UserRepository extends BaseRepository implements UserInterface
{ 
   public function __construct(User $model)
   {
      $this->model=$model;
   }
   
  
   // Inherited
}