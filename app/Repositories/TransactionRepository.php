<?php 

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\TransactionInterface;
use App\Transaction;
use Carbon\Carbon;

class TransactionRepository extends BaseRepository implements TransactionInterface
{ 
   public function __construct(Transaction $model)
   {
      $this->model=$model;
   }
   
   public function getAllFilter($days, $id = 0)
   {
	   $where_filter = [];
	   
	   if($days > 0) {
			$date= Carbon::today();
			$date->subDays($days);
			array_push($where_filter ,['date', '>=', $date->toDateTimeString()]);
	   }
	   
	   if( $id > 0 )
	   {
		    array_push($where_filter,['user_id','=' , $id]);
	   }
		   
	   if(count($where_filter) > 0)
			return  $this->model->where($where_filter)->get();
		else 
			return  $this->model->all();
	   	 
   }
   
 
   
   // Inherited
}