<?php 

namespace App\Repositories;

use App\Repositories\BaseRepositoryInterface;

interface TransactionInterface extends BaseRepositoryInterface
{ 
   public function getAllFilter($days,$id);
}