<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\CategoryInterface;
use App\Category;

class CategoryRepository extends BaseRepository implements CategoryInterface
{ 
  public function __construct(Category $model)
   {
      $this->model=$model;
   }
   
    public function getAllFilter($id)
   {
	   $where_filter = [];
	   
	   if( $id > 0 )
	   {
		    array_push($where_filter,['user_id','=' , $id]);
	   }
		   
	   if(count($where_filter) > 0)
			return  $this->model->where($where_filter)->get();
		else 
			return  $this->model->all();
	   	 
   }
  //Inherited
}