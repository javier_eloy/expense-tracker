<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TransactionRequest;
use App\Repositories\TransactionRepository;
use Carbon\Carbon;

class TransactionController extends Controller
{
	
	private $transaction;
	
	function __construct(TransactionRepository $transaction)
	{
        $this->transaction = $transaction;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return $this->transaction->getAll();
		
    }
	

	 /**
     * Display a listing of the resource at time and user id.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter($days, $id)
    {
		return $this->transaction->getAllFilter($days, $id);
		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request)
    {
        $attributes['subject'] = $request->subject;
        $attributes['amount'] = $request->amount;
        $attributes['type'] = $request->type;
        $attributes['date'] = $request->date;
        $attributes['category_id'] = $request->category_id;
		$attributes['user_id'] = $request->user_id;
		
        $this->transaction->create($attributes);
		
		return response()->json(['data' => $this->transaction->getAll()], 200);
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		return $this->transaction->find($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransactionRequest $request, $id)
    {
	    $attributes['subject'] = $request->subject;
		$attributes['amount'] = $request->amount;
		$attributes['type'] = $request->type;
		$attributes['date'] = $request->date;
		$attributes['category_id'] = $request->category_id;
		$attributes['user_id'] = $request->user_id;
		$attributes['updated_at'] = Carbon::now();
		$this->transaction->update($id,$attributes);
	    		
	    return response()->json(['data' => $this->transaction->getAll()], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->transaction->destroy($id);
		
		return response()->json(['data' => $this->transaction->getAll()], 200);
    }
}
