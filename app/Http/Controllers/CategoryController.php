<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Repositories\CategoryRepository;

class CategoryController extends Controller
{
	
	private $category;
	
	function __construct(CategoryRepository $category)
	{
        $this->category = $category;
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return $this->category->getAll();
    }
	
	 /**
     * Display a listing of the resource at time and user id.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter($id)
    {
		return $this->category->getAllFilter($id);
		
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $attributes['title'] = $request->title;
		$attributes['user_id'] = $request->user_id;
		
        $this->category->create($attributes);
		
		return response()->json(['data' => $this->category->getAll()], 200);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->category->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $attributes['title'] = $request->title;
		$attributes['user_id'] = $request->user_id;
		$this->category->update($id,$attributes);
	     
		 
	    return response()->json(['data' => $this->category->getAll()], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->category->destroy($id);
		
		return response()->json(['data' => $this->category->getAll()], 200);
    }
}
