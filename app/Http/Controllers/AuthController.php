<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Repositories\UserRepository;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
	
	private $user;
	
	function __construct(UserRepository $user)
	{
        $this->user = $user;
    }
	
	
    public function login(Request $request)
	{ 
	  
	    $credential = [
		  'name' => $request->name,
		  'password' => $request->password
		];
		
		if(Auth::once($credential)){ 
		    return Auth::user(); 
		} else { 
		    return 'invalid Username/Password'; 
		} 
	} 
	
	public Function logout()
	{ 
		Auth::logout(); 
		return 'logged out'; 
	} 
	
	 public function register(RegisterRequest $request)
    {
		
		$attributes['name'] = $request->name;
        $attributes['email'] = $request->email;
        $attributes['password'] = Hash::make($request->password);	
        $newUser=$this->user->create($attributes);
		
        if (!$newUser) {
			return response()->json(['failed'],500);
        }
        return $newUser;
    }
}
