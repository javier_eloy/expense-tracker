<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
	protected $table='transaction';
	protected $fillable =['subject','amount','type','category_id','date','user_id'];
	protected $appends = ['type_name', 'category_name'];
	
	public function category()
	{
		 return $this->belongsTo('App\Category');
	}
	
    public function getTypeNameAttribute()
	{
		switch($this->type) {
		   case 0 : return "deposit";
		   case 1 : return "witdrawal";		 
		}
	}
	
	public function getCategoryNameAttribute(){
		 $title= $this->category->attributes['title'];
		 
		 return ($title);		
	}
	
	
	
}
